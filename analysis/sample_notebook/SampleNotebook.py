# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython import get_ipython


# %%
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'analysis/sample_notebook'))
	print(os.getcwd())
except:
	pass

# %% [markdown]
#  <center><img src="https://www.kdnuggets.com/wp-content/uploads/jupyter-logo.jpg" alt="Sample Image height="340" width="480"></center><br>
#  <center><font size="6" face="Roboto"><b>Sample Data Analysis Report</b></font></center>
#  <center><font size="4" face="Roboto">Demonstration Notebook</font></center><br>
#  <center><i><font size="3" face="Roboto">Mike Moore</font></i></center>
# %% [markdown]
#  # Overview
# 
# This report serves as a sample Jupyter Notebook to demonstrate a typical data analysis workflow. More information can be found at the [Jupyter Notebook](https://jupyter.org/) home page.
# %% [markdown]
#  # Analysis Setup and Configuration
#  Our first step is to setup our analysis environment. We do this by importing the Python modules that we will make use of,
#  and loading the data set we will work with. The cell below imports the modules we will need to carry out this analysis.
#  We add the path to our own Python modules by appending to sys.path.

# %%
# Imports all needed Python modules for this notebook.
import sys, os, logging
import seaborn as sns
from os.path import expanduser
from IPython.display import Image
modules_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir, os.pardir, 'src'))
sys.path.append(modules_dir)
from warmups import *

# %% [markdown]
# ## Sample Function Call
# This next code block shows how to call a function that we have defined in our own module (the warmups module).

# %%
square_positive_numbers([3, 4, 5])

# %% [markdown]
#  ## Sample Load Data
#  Once we have imported our needed modules, the next step is to load our data sets. As an example, we load financial data using Yahoo Finance and Quandl.
# 
#  ### Yahoo Finance

# %%
import yfinance as yf
# Get the data for the stock AAPL
data = yf.download('AAPL','2016-01-01','2020-01-09')
# Import the plotting library
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
# Plot the close price of the AAPL
data['Adj Close'].plot()
plt.show()

# %% [markdown]
# ### Quandl
# Be sure to place your Quandl API key instead of "YOUR_KEY_HERE". See [here](https://www.quandl.com) for more information.

# %%
# Import the quandl package
import quandl
# Get the data from quandl
data = quandl.get("WIKI/MSFT", start_date="2016-01-01", end_date="2020-01-09", api_key="YOUR_KEY_HERE")
# Plot the close pr
import matplotlib.pyplot as plt
data.Close.plot()
# Show the plot
plt.show()

# %% [markdown]
#  ## Examine Data
#  At this stage, our variable 'data' contains a Pandas [DataFrame](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html)
#  object containing all of our data. We can used Pandas to examine some of the data that was loaded in to this Jupyter Notebook.

# %%
n_rows = 10
data.head(n_rows)

# %% [markdown]
#  ## Post-Processing
#  Now that we have our data in a convenient structure, we can leverage the full power of Pandas, NumPy, and Matplotlib to process and
#  analyze the data with ease. Of course, we can create functions, classes, and modules defined in separate Python files if we need to do
#  some complex post-processing. This would allow us to share those modules with other team members that may want to re-use and extend them
#  for a similar analysis. Be sure to test drive these modules as you develop them. As we have seen, the interactive nature of Python makes
#  that easy.
# 
#  As a trivial example, here's how to get the maximum closing price of the stock.

# %%
max_closing_price = data['Close'].max()
print("The maximum closing price for the stock in the given data set is : $ {:.2f}.".format(max_closing_price))

# %% [markdown]
#  ## Make Some Simple Plots
#  In addition to using Pandas in combination with NumPy for post-processing, we can also use Pandas in
#  combination with Matplotlib for plotting. Here's an example that you can experiment with.

# %%
width = 20
height = 10
sns.set_context("poster", font_scale=1.1)
fig = plt.figure(figsize=(width, height), dpi=35)
axis = fig.add_subplot(111)
data['Close'].plot(ax=axis)
axis.grid(True)
axis.set_title('Simple Plot')
axis.set_xlabel('Time')
axis.set_ylabel('Closing Price (USD)')
sns.despine()

