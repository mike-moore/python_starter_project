
<center>
<img src="docs/imgs/python.png" alt="Python"/>
</center>

# Welcome to the project
> This is Mike's starter Python project

I use this project as my starting point for any project I develop in Python.

## Getting started
We'll start by creating a workspace directory to hold all course materials. 

```shell
mkdir python_ws
cd python_ws
git clone https://gitlab.com/mike-moore/python_class.git
cd python_class
```

### Python Environments
Create a Python 3 virtual environment called ".venv" in your python_class folder. Virtual environments are 
generally a very good idea when developing your own Python modules and applications. I wish I 
would have known about these when I first started in Python. You can save yourself a lot of headache 
if you 
[read more about virtual environments](https://realpython.com/python-virtual-environments-a-primer/) 
before getting started with Python. The commands below will create and activate the needed environment 
for you. Environments have to be activated on a per shell basis.

```shell
virtualenv -p /usr/bin/python3.6 .venv
source .venv/bin/activate
```

Our next step is installing the Python modules needed for this course. The requirements.txt file in 
the env directory of this repository defines these dependencies. Defining a requirements.txt file is 
a best practice for Python development. The last step uses pip to install these packages 
into our newly created virtual environment. Pip is Python's package manager. You can [read more about 
pip here](https://realpython.com/what-is-pip/).

```shell
pip install -r env/requirements.txt
```

### Automated Tests With Nosetest
At this point you should be able to run this project's unit tests to confirm that your environment 
is working. 

```shell
cd tests
nosetests
```

You should expect to see the following output:

```shell
.......
----------------------------------------------------------------------
Ran 13 tests in 3.624s

OK
```

This project uses the Python unittest module and nosetest for running our tests. You can 
learn more about test driven development with the 
<a href="https://www.freecodecamp.org/news/learning-to-test-with-python-997ace2d8abe/" target="_blank">Python unittest module here</a>.
You can also
<a href="http://pythontesting.net/framework/nose/nose-introduction/" target="_blank">read more about nose here</a>.

### VS Code Development Environment
We highly recommend the use of the 
[VS Code IDE](https://code.visualstudio.com/docs/python/python-tutorial) 
to better facilitate test driven development and the creation of 
[Jupyter Notebooks](https://jupyter.org/). There are many other useful Python IDEs out there, but 
VS Code is a great free one that's cross platform. You can install and launch it using the steps below.

```shell
cd ~/python_ws
wget -O VScode.tar.gz https://go.microsoft.com/fwlink/?LinkID=620884
tar -xvf VScode.tar.gz
cd python_class
../VSCode-linux-x64/bin/code .
```

Next, [install the Python extension for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-python.python). 
Make sure that the selected Python interpreter is the one found in your newly created .venv folder. Follow these steps 
if you need help [setting the active Python interpreter for your VS Code project](https://code.visualstudio.com/docs/python/python-tutorial#_select-a-python-interpreter). 
Now install the test framework and pylint. VS code should prompt you to install both during the first scan of our project in the bottom right of the window. 
Finally, install the VS Code Jupyter Notebook Previewer extension using the VS code extensions manager.
