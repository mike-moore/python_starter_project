from unittest import TestCase
import os, sys, logging, unittest
# Setup logger    
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, "\033[93m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.ERROR, "\033[91m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)

class TestWarmUps(TestCase):

    def test_add_two_numbers(self):
        from warmups import add_two_numbers
        self.assertAlmostEqual(6.0, add_two_numbers(2.0, 4.0), 4)
        return

    def test_concatenate_two_lists(self):
        from warmups import concatenate_two_lists
        list_a = ['Red', 'Blue', 'Green']
        list_b = ['Yellow', 'Orange']
        expected_list = ['Red', 'Blue', 'Green', 'Yellow', 'Orange']
        self.assertEqual(expected_list, concatenate_two_lists(list_a, list_b))
        return
    
    def test_square_numbers(self):
        from warmups import square_positive_numbers
        first_list_numbers = [4, 0.25, 24, 35]
        first_expected = [16, 0.0625, 576, 1225]
        second_list_numbers = [102, 33.25, 19, 0.5]
        second_expected = [10404, 1105.5625, 361, 0.25]
        self.assertEqual(first_expected, square_positive_numbers(first_list_numbers))
        self.assertEqual(second_expected, square_positive_numbers(second_list_numbers))
    
    def test_error_handling(self):
        from warmups import square_positive_numbers
        list_negative_numbers = [-2.0, -16.0, -5.0]
        with self.assertRaises(ValueError):
            square_positive_numbers(list_negative_numbers)
    
    def test_calculate_average(self):
        from warmups import calculate_average
        test_number_set1 = [5.0, 10.0, 15.0]
        self.assertAlmostEqual(10.0, calculate_average(test_number_set1), 4)
        test_number_set2 = [10.0, 21.0, 46.0, -18.0, 97.0]
        self.assertAlmostEqual(31.20, calculate_average(test_number_set2), 4)
        test_number_set3 = range(1, 1000)
        self.assertAlmostEqual(500.0, calculate_average(test_number_set3), 4)

    def test_remove_duplicates_from_list(self):
        from warmups import remove_duplicates_from_list
        test_number_list = [5.0, 15.0, 0.5, 5.0, 15.0]
        self.assertEqual([5.0, 15.0, 0.5], remove_duplicates_from_list(test_number_list))
        test_string_list = ['red', 'red', 'green', 'blue', 'green']
        self.assertEqual(['red', 'green', 'blue'], remove_duplicates_from_list(test_string_list))

if __name__ == '__main__':
    unittest.main()
